#include "Crystal.h"

Crystal::Crystal()
{
}

Crystal::Crystal(string name, int chance)
{
	mName = name;
	mChance = chance;
}

Crystal::~Crystal()
{
}

void Crystal::effect(Player* player)
{
	cout << "You received 15 Crystals!" << endl;
	player->addCrystals(15);
}
