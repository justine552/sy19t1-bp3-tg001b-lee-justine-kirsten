#include "SR.h"

SR::SR()
{
}

SR::SR(string name, int chance)
{
	mName = name;
	mChance = chance;
}

SR::~SR()
{
}

void SR::effect(Player* player)
{
	cout << "You got 10 Rarity Points!" << endl;
	player->addRarityPt(10);
}
