	#pragma once
#include <iostream>
#include <string>
#include "Player.h"
using namespace std;

class Item
{
	public:
		Item();
		Item(string name, int chance);
		~Item();

		int getChance();
		string getName();
		virtual void effect(Player* player);
	protected:
		string mName;
		int mChance;
};

