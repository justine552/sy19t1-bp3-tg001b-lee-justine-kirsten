#include "HealthPotion.h"

HealthPotion::HealthPotion()
{
}

HealthPotion::HealthPotion(string name, int chance)
{
	mName = name;
	mChance = chance;
}

HealthPotion::~HealthPotion()
{
}

void HealthPotion::effect(Player* player)
{
	cout << "You got healed for 30 HP!" << endl;
	player->addHp(30);
}
