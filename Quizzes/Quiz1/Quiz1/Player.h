#pragma once
#include <iostream>
#include <string>
using namespace std;

class Player
{
	public:
		Player();
		Player(int hp, int crystals, int rarityPts, int pulls);
		~Player();

		int getHp();
		int getCrystals();
		int getRarityPts();
		int getPulls();
		void pull();
		void addHp(int value);
		void reduceHp(int value);
		void addRarityPt(int value);
		void addCrystals(int value);
		void reduceCrystals(int value);
		void addPulls(int value);
	private:
		int mHp;
		int mCrystals;
		int mRarityPts;
		int mPulls;
};

