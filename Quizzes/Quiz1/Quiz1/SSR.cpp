#include "SSR.h"

SSR::SSR()
{
}

SSR::SSR(string name, int chance)
{
	mName = name;
	mChance = chance;
}

SSR::~SSR()
{
}

void SSR::effect(Player* player)
{
	cout << "You got 50 Rarity Points!" << endl;
	player->addRarityPt(50);
}
