#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;

class SR : public Item
{
	public:
		SR();
		SR(string name, int chance);
		~SR();

		virtual void effect(Player* player);
	private: 
		string mName;
		int mChance;

};
