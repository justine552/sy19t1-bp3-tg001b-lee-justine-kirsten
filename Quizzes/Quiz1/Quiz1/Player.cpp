#include "Player.h"

Player::Player()
{
}

Player::Player(int hp, int crystals, int rarityPts, int pulls)
{
	mHp = hp;
	mCrystals = crystals;
	mRarityPts = rarityPts;
	mPulls = pulls;
}

Player::~Player()
{
}

int Player::getHp()
{
	return mHp;
}

int Player::getCrystals()
{
	return mCrystals;
}

int Player::getRarityPts()
{
	return mRarityPts;
}

int Player::getPulls()
{
	return mPulls;
}

void Player::pull()
{

}

void Player::addHp(int value)
{
	mHp += value;
}

void Player::reduceHp(int value)
{
	mHp -= value;
}

void Player::addRarityPt(int value)
{
	mRarityPts += value;
}

void Player::addCrystals(int value)
{
	mCrystals += value;

}

void Player::reduceCrystals(int value)
{
	mCrystals -= value;
}

void Player::addPulls(int value)
{
	mPulls += value;
}
