#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;

class Bomb : public Item
{
	public:
		Bomb();
		Bomb(string name, int chance);
		~Bomb();

		virtual void effect(Player* player);
	private:
};

