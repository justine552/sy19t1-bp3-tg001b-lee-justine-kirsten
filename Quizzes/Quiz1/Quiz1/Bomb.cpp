#include "Bomb.h"

Bomb::Bomb()
{
}

Bomb::Bomb(string name, int chance)
{
	mName = name;
	mChance = chance;
}

Bomb::~Bomb()
{
}

void Bomb::effect(Player* player)
{
	cout << "You received 25 damage!" << endl;
	player->reduceHp(25);
}
