#include "Item.h"

Item::Item()
{
}

Item::Item(string name, int chance)
{
	mName = name;
	mChance = chance;
}

Item::~Item()
{
}

int Item::getChance()
{
	return mChance;
}

string Item::getName()
{
	return mName;
}

void Item::effect(Player* player)
{

}

