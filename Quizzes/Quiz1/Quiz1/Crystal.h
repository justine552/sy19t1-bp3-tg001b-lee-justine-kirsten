#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;

class Crystal : public Item
{
	public:
		Crystal();
		Crystal(string name, int chance);
		~Crystal();

		virtual void effect(Player* player);
	private:
};

