#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;

class SSR : public Item
{
	public:
		SSR();
		SSR(string name, int chance);
		~SSR();

		virtual void effect(Player* player);	
	private:
};

