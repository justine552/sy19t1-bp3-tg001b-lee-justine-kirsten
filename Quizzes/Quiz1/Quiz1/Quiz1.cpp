#include <iostream>
#include <string>
#include <vector>
#include "Bomb.h"
#include "Item.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"
#include "Crystal.h"
#include "HealthPotion.h"
#include "Player.h"
#include <ctime>
using namespace std;

Player* createPlayer()
{
	return new Player(100, 100, 0, 0);
}

Item* spawnItem()
{
	Item** items = new Item * [6];
	items[0] = new SSR("SSR", 1);
	items[1] = new SR("SR", 9);
	items[2] = new R("R", 40);
	items[3] = new HealthPotion("Health Potion", 15);
	items[4] = new Bomb("Bomb", 20);
	items[5] = new Crystal("Crystal", 15);

	int spawnChance = 0;
	for (int i = 0; i < 6; i++)
	{
		spawnChance += items[i]->getChance();
	}

	for (int i = 0; i < 6; i++)
	{
		int randomNum = rand() % spawnChance;

		if (randomNum < items[i]->getChance())
		{
			return items[i];
		}
		else
		{
			spawnChance -= items[i]->getChance();
		}
	}

	return NULL;
}

void printStats(Player* player)
{
	cout << "HP: " << player->getHp() << endl;
	cout << "Crystals: " << player->getCrystals() << endl;
	cout << "Rarity Points: " << player->getRarityPts() << endl;
	cout << "Pulls: " << player->getPulls() << endl;
}

void printGameSummary(vector<Item*> itemsPulled)
{
	for (int i = 0; i < itemsPulled.size(); i++)
	{
		cout << itemsPulled[i]->getName() << endl;
	}
}

int main()
{
	srand(time(NULL));
	Player* player = createPlayer();
	vector<Item*> itemsPulled;


	while (player->getHp() > 0 && player->getCrystals() > 0 && player->getRarityPts() < 100)
	{
		printStats(player);
		Item* itemPulled = spawnItem();
		cout << "You pulled " << itemPulled->getName() << endl;
		player->reduceCrystals(5);
		itemPulled->effect(player);
		cout << endl;
		player->addPulls(1);
		itemsPulled.push_back(itemPulled);

		system("pause");
		cout << endl;
	}
	if (player->getRarityPts() >= 100)
	{
		cout << "You won!" << endl;
		printGameSummary(itemsPulled);
		system("pause");
	}
	else if (player->getHp() <= 0 || player->getCrystals() <= 0)
	{
		cout << "You lost!" << endl;
		printGameSummary(itemsPulled);
		system("pause");
	}

	system("pause");
	return 0;
}