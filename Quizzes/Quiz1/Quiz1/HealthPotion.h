#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;

class HealthPotion : public Item
{
	public:
		HealthPotion();
		HealthPotion(string name, int chance);
		~HealthPotion();

		virtual void effect(Player* player);
	private:


};


