#include "R.h"

R::R()
{
}

R::R(string name, int chance)
{
	mName = name;
	mChance = chance;
}

R::~R()
{
}

void R::effect(Player* player)
{
	cout << "You got 1 Rarity Point!" << endl;
	player->addRarityPt(1);
}
