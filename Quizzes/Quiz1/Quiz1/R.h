#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;

class R : public Item
{
	public: 
		R();
		R(string name, int chance);
		~R();

		virtual void effect(Player* player);
	private:


};

