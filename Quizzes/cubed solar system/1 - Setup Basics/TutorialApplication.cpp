/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h> // Remember to include these two
#include <vector>

TutorialApplication::TutorialApplication(void)
{
	planets = std::vector<Planet*>();
}
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	float defaultNumOfDaysPerRevolution = 365.20; //earth
	float defaultNumOfDaysPerRotation = 0.99726851f; //earth

	Planet* sun = Planet::createPlanet(mSceneMgr, 20, ColourValue(252.0f / 255.0f, 212.0f / 255.0f, 64.0f / 255.0f));
	sun->getNode().setPosition(0, 0.5, 0);
	sun->setLocalRotationSpeed(defaultNumOfDaysPerRotation / 24.0f);

	Planet* mercury = Planet::createPlanet(mSceneMgr, 3, ColourValue(181.0f/255.0f, 167.0f / 255.0f, 167.0f / 255.0f));
	mercury->getNode().setPosition(100, 0.5, 0);
	mercury->setParent(sun);
	mercury->setLocalRotationSpeed(defaultNumOfDaysPerRotation / 59.0f);
	mercury->setRevolutionSpeed(defaultNumOfDaysPerRevolution / 88.0f);

	Planet* venus = Planet::createPlanet(mSceneMgr, 5, ColourValue(139.0f / 255.0f, 125.0f / 255.0f, 130.0f / 255.0f));
	venus->getNode().setPosition(200, 0.5, 0);
	venus->setParent(sun);
	venus->setLocalRotationSpeed(-defaultNumOfDaysPerRotation / 243.0f);
	venus->setRevolutionSpeed(defaultNumOfDaysPerRevolution / 224.7f);

	Planet* earth = Planet::createPlanet(mSceneMgr, 10, ColourValue(59.0f / 255.0f, 93.0f / 255.0f, 56.0f / 255.0f));
	earth->getNode().setPosition(300, 0.5, 0);
	earth->setParent(sun);
	earth->setLocalRotationSpeed(defaultNumOfDaysPerRotation / 0.99726851f);
	earth->setRevolutionSpeed(defaultNumOfDaysPerRevolution / 365.20f);

	Planet* moon = Planet::createPlanet(mSceneMgr, 1, ColourValue(254.0f / 255.0f, 252.0f / 255.0f, 215.0f / 255.0f));
	moon->getNode().setPosition(350, 0.5, 0);
	moon->setParent(earth);
	moon->setLocalRotationSpeed(0.0f);
	moon->setRevolutionSpeed(5.0);

	Planet* mars = Planet::createPlanet(mSceneMgr, 8, ColourValue(193.0f / 255.0f, 68.0f / 255.0f, 14.0f / 255.0f));
	mars->getNode().setPosition(450, 0.5, 0);
	mars->setParent(sun);
	mars->setLocalRotationSpeed(defaultNumOfDaysPerRotation / 1.0256944f);
	mars->setRevolutionSpeed(defaultNumOfDaysPerRevolution / 687.0f);

	planets.push_back(sun);
	planets.push_back(mercury);
	planets.push_back(venus);
	planets.push_back(earth);
	planets.push_back(moon);
	planets.push_back(mars);



	/*Planet* planet0 = Planet::createPlanet(mSceneMgr, 20, ColourValue::Red);

	Planet* planet1 = Planet::createPlanet(mSceneMgr, 10, ColourValue::Blue);
	planet1->getNode().setPosition(80, 0.5, 5);*/


	// First you have to create an object in the scene
	//ManualObject* object = mSceneMgr->createManualObject(); // don't do new ManualObject()
	// Begin drawing the object
	// The following code means: 
	// Display my object without any lighting
	// and with a base color of white
	// Using a triangle list (3 vertices = 1 triangle)
	//object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	//SceneNode* mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	//Planet* sun = new Planet(mNode);
	//mNode->setPosition(0, 0.5, 0);
	//mNode->attachObject(sun->createCubeMesh("Cube", 20, ColourValue::Red));

	//SceneNode* cubeNode =  mSceneMgr->getRootSceneNode()->createChildSceneNode();
	//Planet* earth = new Planet(cubeNode);
	//cubeNode->setPosition(80, 0.5, 5);
	//cubeNode->attachObject(earth->createCubeMesh("Cube", 10, ColourValue::Blue));

		// After drawing, you need to END the drawing
		//object->end();
		//// After ending the drawing, you need to add the object to the scene
		//cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		//cubeNode->attachObject(object);
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	for (int i = 0; i < planets.size(); i++)
	{
		planets[i]->update(evt);
	}


	//if (mKeyboard->isKeyDown(OIS::KC_I))
	//{
	//	int speed = 10;
	//	cubeNode->translate(speed * evt.timeSinceLastFrame, 0, -1);
	//	speed += 10 * evt.timeSinceLastFrame;
	//}

	//else if (mKeyboard->isKeyDown(OIS::KC_K))
	//{
	//	int speed = 10;
	//	cubeNode->translate(speed * evt.timeSinceLastFrame, 0, 1);
	//	speed += 10 * evt.timeSinceLastFrame;
	//}

	//else if (mKeyboard->isKeyDown(OIS::KC_J))
	//{
	//	int speed = 10;
	//	cubeNode->translate(-1, speed * evt.timeSinceLastFrame, 0);
	//	speed += 10 * evt.timeSinceLastFrame;
	//}
	//else if (mKeyboard->isKeyDown(OIS::KC_L))
	//{
	//	int speed = 10;
	//	cubeNode->translate(1, speed * evt.timeSinceLastFrame, 0);
	//	speed += 10 * evt.timeSinceLastFrame;
	//}
	//return true;

	//if (mKeyboard->isKeyDown(OIS::KC_NUMPAD0))
	//{
	//	Degree rotation = Degree(30 * evt.timeSinceLastFrame);
	//	float newX = ((cubeNode->getPosition().x * cos(1)) + (cubeNode->getPosition().z * sin(1)));
	//	float newZ = ((cubeNode->getPosition().x * sin(1)) + (cubeNode->getPosition().z * cos(1)));
	//	//float newY = newX + newZ;
	//	cubeNode->setPosition(Vector3(newX, 0, newZ));
	//	//cubeNode->rotate(Vector3(newX * evt.timeSinceLastFrame, newY * evt.timeSinceLastFrame, newZ * evt.timeSinceLastFrame), rotation);
	//}

	return true;
}

//
//
//// Sample movement code
//bool TutorialApplication::keyPressedI(const OIS::KeyEvent& arg)
//{
//	if (arg.key == OIS::KC_I)
//	{
//		cubeNode->translate(0, 0, -1);
//	}
//
//	return true;
//}
//
//bool TutorialApplication::keyReleasedI(const OIS::KeyEvent& arg)
//{
//	return true;
//}
//
//bool TutorialApplication::keyPressedK(const OIS::KeyEvent& arg)
//{
//	if (arg.key == OIS::KC_K)
//	{
//		cubeNode->translate(0, 0, 1);
//	}
//	return true;
//}
//	
//bool TutorialApplication::keyReleasedK(const OIS::KeyEvent& arg)
//{
//	return true;
//}
//
//bool TutorialApplication::keyPressedJ(const OIS::KeyEvent& arg)
//{
//	if (arg.key == OIS::KC_J)
//	{
//		cubeNode->translate(-1, 0, 0);
//	}
//	return true;
//}
//
//bool TutorialApplication::keyReleasedJ(const OIS::KeyEvent& arg)
//{
//	return true;
//}
//
//bool TutorialApplication::keyPressedL(const OIS::KeyEvent& arg)
//{
//	if (arg.key == OIS::KC_L)
//	{
//		cubeNode->translate(1, 0, 0);
//	}
//	return true;
//}
//
//bool TutorialApplication::keyReleasedL(const OIS::KeyEvent& arg)
//{
//	return true;
//}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception & e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
	}

		return 0;
}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
