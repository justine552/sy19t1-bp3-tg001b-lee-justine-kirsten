#pragma once
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;

class Planet
{
public: 
	static Planet* createPlanet(SceneManager* sceneManager, float size, ColourValue colour);
	Planet(SceneNode* node);
	virtual ~Planet();

	void update(const FrameEvent& evt);

	SceneNode &getNode();
	void setParent(Planet* parent);
	Planet* getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);
private:
	SceneNode* mNode;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;
	float speedOfTimePerCycleInSeconds;
};

