#include "Planet.h"
#include <string> 

using namespace std;

Planet* Planet::createPlanet(SceneManager* sceneManager, float size, ColourValue colour)
{
	ManualObject* newPlanet = sceneManager->createManualObject();

	newPlanet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	newPlanet->colour(colour);

	//front square
	newPlanet->position(-size, -size, size);
	newPlanet->position(size, -size, size);
	newPlanet->position(size, size, size);
	newPlanet->position(size, size, size);
	newPlanet->position(-size, size, size);
	newPlanet->position(-size, -size, size);

	//back square
	newPlanet->position(size, -size, -size);
	newPlanet->position(-size, -size, -size);
	newPlanet->position(-size, size, -size);
	newPlanet->position(-size, size, -size);
	newPlanet->position(size, size, -size);
	newPlanet->position(size, -size, -size);

	//left square
	newPlanet->position(-size, -size, -size);
	newPlanet->position(-size, -size, size);
	newPlanet->position(-size, size, size);
	newPlanet->position(-size, size, size);
	newPlanet->position(-size, size, -size);
	newPlanet->position(-size, -size, -size);

	//right square
	newPlanet->position(size, -size, size);
	newPlanet->position(size, -size, -size);
	newPlanet->position(size, size, -size);
	newPlanet->position(size, size, -size);
	newPlanet->position(size, size, size);
	newPlanet->position(size, -size, size);

	//top square
	newPlanet->position(-size, size, size);
	newPlanet->position(size, size, size);
	newPlanet->position(size, size, -size);
	newPlanet->position(size, size, -size);
	newPlanet->position(-size, size, -size);
	newPlanet->position(-size, size, size);

	//bottom square
	newPlanet->position(size, -size, size);
	newPlanet->position(-size, -size, size);
	newPlanet->position(-size, -size, -size);
	newPlanet->position(-size, -size, -size);
	newPlanet->position(size, -size, -size);
	newPlanet->position(size, -size, size);

	// After drawing, you need to END the drawing
	newPlanet->end();

	SceneNode* newChildNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	newChildNode->attachObject(newPlanet);

	return new Planet(newChildNode);
}


Planet::Planet(SceneNode* node)
{
	mNode = node;
	mLocalRotationSpeed = 0;
	mRevolutionSpeed = 0;
	speedOfTimePerCycleInSeconds = 60.0f;
}

Planet::~Planet()
{
	// Hello World
}

void Planet::update(const FrameEvent& evt)
{
	// local rotation
	float secondPerCycle = speedOfTimePerCycleInSeconds / 60.0f;
	Degree yawDegrees = Degree(((360.0f / secondPerCycle) * mLocalRotationSpeed) * evt.timeSinceLastFrame);
	Radian yawRadian = Radian(yawDegrees);
	mNode->yaw(yawRadian);

	if (mParent)  
	{
		// revolution
		/*Vector3 parentPos = mParent->getNode().getPosition();
		Vector3 curPos = mNode->getPosition();*/

		/*float normalizedRevSpeed = mRevolutionSpeed * evt.timeSinceLastFrame;

		float xOrigDelta = curPos.x - parentPos.x;
		float zOrigDelta = curPos.z - parentPos.z;

		float newX = (curPos.x * cos(normalizedRevSpeed)) + (curPos.z * sin(normalizedRevSpeed));
		float newZ = (curPos.x * -sin(normalizedRevSpeed)) + (curPos.z * cos(normalizedRevSpeed));

		float xNewDelta = newX - parentPos.x;
		float zNewDelta = newZ - parentPos.z;*/

		//////////////////
		float oneRevolutionInDegrees = 360.0f;
		Degree degrees = Degree(((oneRevolutionInDegrees / speedOfTimePerCycleInSeconds) * mRevolutionSpeed) * evt.timeSinceLastFrame);
		Radian radian = Radian(degrees);
		float angle = radian.valueRadians();

		Vector3 point = mNode->getPosition();
		Vector3 center = mParent->getNode().getPosition();

		float rotatedX = cos(angle) * (point.x - center.x) - sin(angle) * (point.z - center.z) + center.x;
		float rotatedZ = sin(angle) * (point.x - center.x) + cos(angle) * (point.z - center.z) + center.z;

		mNode->setPosition(rotatedX, point.y, rotatedZ);
	}
}

SceneNode& Planet::getNode()
{
	return *mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}
