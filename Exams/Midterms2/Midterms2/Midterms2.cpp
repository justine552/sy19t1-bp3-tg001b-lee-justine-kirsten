#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include "Unit.h"
#include "AI.h"
#include "Player.h"
using namespace std;

vector<Unit*> createPlayer()
{
	vector<Unit*> playerArray;
	playerArray.push_back(new Player("E.T.C", new Stats(45, 45, 12, 12, 26, 30, 15, 18), new Class(Warrior)));
	playerArray.push_back(new Player("Illidan", new Stats(35, 42, 13, 13, 15, 20, 25, 20), new Class(Assassin)));
	playerArray.push_back(new Player("Lili", new Stats(30, 30, 28, 28, 15, 20, 18, 20), new Class(Mage)));
	return playerArray;
}

vector<Unit*> createAI()
{
	vector<Unit*> AIArray;
	AIArray.push_back(new AI("Johanna", new Stats(36, 36, 15, 15, 13, 14, 25, 20), new Class(Assassin)));
	AIArray.push_back(new AI("Valla", new Stats(36, 36, 25, 25, 13, 20, 16, 20), new Class(Mage)));
	AIArray.push_back(new AI("Khazarim", new Stats(42, 42, 14, 14, 25, 28, 17, 19), new Class(Warrior)));
	return AIArray;
}

int getTotalHp(vector<Unit*> unitArray)
{
	int totalHp = 0;

	for (int i = 0; i < unitArray.size(); i++)
	{
		if (unitArray[i]->getStats()->getHp() > 0)
		{
			totalHp += unitArray[i]->getStats()->getHp();
		}
	}
	return totalHp;
}

void displayTeamStats(vector<Unit*> unitArray, string teamName)
{
	cout << "==================================" << endl;
	cout << "Team: " << teamName << endl;
	cout << "==================================" << endl;

	for (int i = 0; i < unitArray.size(); i++)
	{
		if (unitArray[i]->getStats()->getHp() > 0)
		{

			cout << unitArray[i]->getName() << "[HP: " << unitArray[i]->getStats()->getHp() << "]" << endl;
		}
	}
}

vector<Unit*> bubbleSort(vector<Unit*> unitArray)
{
	for (int i = 0; i < unitArray.size(); i++)
	{
		for (int j = 0; j < (unitArray.size() - 1); j++)
		{
			if (unitArray[j]->getStats()->getAgi() > unitArray[j + 1]->getStats()->getAgi())
			{
				Unit* temp = unitArray[j];
				unitArray[j] = unitArray[j + 1];
				unitArray[j + 1] = temp;
			}
		}
	}
	return unitArray;
}

void displayMemberStats(Unit* unit)
{
	cout << "Name: " << unit->getName() << endl;
	cout << "Class: " << unit->getClass()->getClassName() << endl;
	cout << "HP: " << unit->getStats()->getHp() << "/" << unit->getStats()->getMaxHp() << endl;
 	cout << "MP: " << unit->getStats()->getMp() << "/" << unit->getStats()->getMaxMp() << endl;
	cout << "Vit: " << unit->getStats()->getVit() << endl;
	cout << "Dex: " << unit->getStats()->getDex() << endl;
	cout << "Agi: " << unit->getStats()->getAgi() << endl;
}

void inputSkill(Unit* unit, vector<Unit*> targets)
{
	int choice = 0;

	if (typeid(*unit) == typeid(Player))
	{
		unit->processTurn(targets, unit, choice);
	}
	else if (typeid(*unit) == typeid(AI))
	{
		vector<Skill*> unitSkills = unit->getSkills();
		choice = rand() % 2;
		cout << "[" << choice << "]" << " " << unitSkills[choice]->getName() << endl;
		unit->processTurn(targets, unit, choice);
	}


}

void checkMembersHp(vector<Unit*>* targets, vector<Unit*>* team)
{
	for (int i = targets->size() - 1; i > 0; i--)
	{
		if ((*targets)[i]->getStats()->getHp() <= 0)
		{
			Unit* target = (*targets)[i];
			(*targets).erase(remove((*targets).begin(), (*targets).end(), target), (*targets).end());


			(*team).erase(remove((*team).begin(), (*team).end(), target), (*team).end());
		}
	}
}

void checkWin(vector<Unit*> targets, vector<Unit*> team)
{

	if (typeid(*targets[0]) != typeid(Player))
	{
		cout << "Warcraft wins!" << endl;
	}
	else
	{
		cout << "Diablo win!" << endl;
	}
}

int main()
{
	srand(time(NULL));
	vector<Unit*> playerUnits = createPlayer();
	vector<Unit*> AIUnits = createAI();
	vector<Unit*> allUnits;

	for (int i = 0; i < playerUnits.size(); i++)
	{
		allUnits.push_back(playerUnits[i]);
	}
	for (int i = 0; i < AIUnits.size(); i++)
	{
		allUnits.push_back(AIUnits[i]);
	}

	allUnits = bubbleSort(allUnits);

	while (getTotalHp(playerUnits) > 0 && getTotalHp(AIUnits) > 0)
	{
		checkMembersHp(&allUnits, &playerUnits);
		checkMembersHp(&allUnits, &AIUnits);

		displayTeamStats(playerUnits, "Warcraft");
		cout << endl;
		displayTeamStats(AIUnits, "Diablo");
		cout << endl;

		for (int i = 0; i < allUnits.size(); i++)
		{
			cout << "[" << i + 1 << "] " << allUnits[i]->getName() << endl;
		}
		system("pause");
		system("cls");

		Unit* currentUnitTurn = allUnits[0];
		allUnits.erase(allUnits.begin());

		displayMemberStats(currentUnitTurn);

		inputSkill(currentUnitTurn, allUnits);

		allUnits.push_back(currentUnitTurn);

		if (getTotalHp(playerUnits) <= 0 || getTotalHp(AIUnits) <= 0)
		{
			checkWin(AIUnits, playerUnits);
			system("pause");
			break;
		}

		cout << endl;
		system("pause");
		system("cls");
	}




	return 0;
}
