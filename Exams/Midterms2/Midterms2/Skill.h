#ifndef SKILL_H

#define SKILL_H
#include <string>
#include <vector>
//#include "Unit.h"
using namespace std;

class Unit;

class Skill
{
public:
	Skill();
	~Skill();

	string getName();
	int getMpCost();

	virtual void castSkill(Unit* attacker, vector<Unit*> targets);
protected:
	string mName;
	int mDamageCoefficient;
	int mCritChance;
	int mMpCost;
	int mMissChance;
};
#endif
