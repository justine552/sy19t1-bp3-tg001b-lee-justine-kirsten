#ifndef STATS_H

#define STATS_H
#include <string>
using namespace std;

class Stats
{
	public:
		Stats(int hp, int maxHp, int maxMp, int mp, int pow, int vit, int agi, int dex);
		~Stats();

		int getHp();
		int getMaxHp();
		int getMaxMp();
		int getMp();
		int getPow();
		int getVit();
		int getAgi();
		int getDex();

		int setHp(int value);
		int setMp(int value);
		int setPow(int value);
		int setVit(int value);
		int setAgi(int value);
		int setDex(int value);

	private:
		int mHp;
		int mMaxHp;
		int mMaxMp;
		int mMp;
		int mPow;
		int mVit;
		int mAgi;
		int mDex;
};
#endif
