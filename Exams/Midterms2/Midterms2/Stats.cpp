#include "Stats.h"

Stats::Stats(int hp, int maxHp, int maxMp, int mp, int pow, int vit, int agi, int dex)
{
	mHp = hp;
	mMaxHp = maxHp;
	mMaxMp = maxMp;
	mMp = mp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
}

Stats::~Stats()
{
	delete &mHp;
	delete &mMaxHp;
	delete &mMaxMp;
	delete &mMp;
	delete &mPow;
	delete &mVit;
	delete &mAgi;
	delete &mDex;
}

int Stats::getHp()
{
	return mHp;
}

int Stats::getMaxHp()
{
	return mMaxHp;
}

int Stats::getMaxMp()
{
	return mMaxMp;
}

int Stats::getMp()
{
	return mMp;
}

int Stats::getPow()
{
	return mPow;
}

int Stats::getVit()
{
	return mVit;
}

int Stats::getAgi()
{
	return mAgi;
}

int Stats::getDex()
{
	return mDex;
}

int Stats::setHp(int value)
{
	if (value > mMaxHp)
	{
		value = mMaxHp;
	}
	mHp = value;
	return mHp;
}

int Stats::setMp(int value)
{
	mMp = value;
	return mMp;
}

int Stats::setPow(int value)
{
	mPow = value;
	return mPow;
}

int Stats::setVit(int value)
{
	mVit = value;
	return mVit;
}

int Stats::setAgi(int value)
{
	mAgi = value;
	return mAgi;
}

int Stats::setDex(int value)
{
	mDex = value;
	return mDex;
}
