#ifndef SHOCKWAVE_H

#define SHOCKWAVE_H
#include <string>
#include <vector>
#include "Skill.h"
#include "Unit.h"
using namespace std;

class Shockwave : public Skill
{
	public:
		Shockwave();
		Shockwave(string name);
		~Shockwave();

		virtual void castSkill(Unit* attacker, vector<Unit*> targets);
	private:
};
#endif
