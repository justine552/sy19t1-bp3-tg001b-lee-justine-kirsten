#include "BasicAttack.h"
#include "Player.h"

BasicAttack::BasicAttack()
{
}

BasicAttack::BasicAttack(string name)
{
	mName = name;
	mMpCost = 0;
}

BasicAttack::~BasicAttack()
{
}

void BasicAttack::castSkill(Unit* attacker, vector<Unit*> targets)
{

	//Remove all player from targets
	for (int i = targets.size()-1; i >= 0; i--)
	{
		if (typeid(*attacker) == typeid(*targets[i]))
		{
			auto newEnd = std::remove(targets.begin(), targets.end(), targets[i]);
			targets.erase(newEnd, targets.end());
		}
	}
	// Get random target from remaining targets
	Unit* target;
	int randIndexTarget = rand() % targets.size();
	target = targets[randIndexTarget];

	// Compute for damage
	int hitRate = (attacker->getStats()->getDex() / target->getStats()->getAgi()) * 100;

	int power = attacker->getStats()->getPow() + ((attacker->getStats()->getPow() * 0.2) - 1);

	int randomPow = rand() % power + attacker->getStats()->getPow();

	int baseDamage = randomPow * 1.0f;

	int actualDamage = (baseDamage - target->getStats()->getVit());

	if (actualDamage < 0)
		actualDamage = 0;

	if (hitRate > 80)
	{
		hitRate = 80;
	}
	if (hitRate < 20)
	{
		hitRate = 20;
	}

	int randomNum = rand() % 100+1;

	int bonusDamage = 1;

	if ((attacker->getClass()->getClassType() == Warrior &&
		target->getClass()->getClassType() == Assassin))
	{
		bonusDamage = 1.5f;
	}
	else if (attacker->getClass()->getClassType() == Assassin &&
		target->getClass()->getClassType() == Mage)
	{
		bonusDamage = 1.5f;
	}
	else if (attacker->getClass()->getClassType() == Mage &&
		target->getClass()->getClassType() == Warrior)
	{
		bonusDamage = 1.5f;
	}

	actualDamage *= bonusDamage;

	if(randomNum > hitRate)
	{
		cout << "Miss!" << endl;
	}
	else
	{
		cout << "Hit " << target->getName() << " for " << actualDamage << " damage!" << endl;
		target->receiveDamage(actualDamage);
	}




}
