#include "Assassinate.h"

Assassinate::Assassinate()
{
}

Assassinate::Assassinate(string name)
{
	mName = name;
	mMpCost = 5;
}

Assassinate::~Assassinate()
{
}

void Assassinate::castSkill(Unit* attacker, vector<Unit*> targets)
{
	for (int i = targets.size() - 1; i >= 0; i--)
	{
		if (typeid(*attacker) == typeid(*targets[i]))
		{
			auto newEnd = std::remove(targets.begin(), targets.end(), targets[i]);
			targets.erase(newEnd, targets.end());
		}
	}

	Unit* target = targets[0];
	for (int i = 0; i < targets.size(); i++)
	{
		if (target->getStats()->getHp() > targets[i]->getStats()->getHp())
		{
			target = targets[i];
		}
	}

	int power = ((attacker->getStats()->getPow() * 0.2) - 1);
	int randomPow = rand() % power + attacker->getStats()->getPow();
	int baseDamage = randomPow * 2.2f;
	int actualDamage = (baseDamage - target->getStats()->getVit());

	int bonusDamage = 1;

	if ((attacker->getClass()->getClassType() == Warrior &&
		target->getClass()->getClassType() == Assassin))
	{
		bonusDamage = 1.5f;
	}
	else if ((attacker->getClass()->getClassType() == Assassin &&
		target->getClass()->getClassType() == Mage))
	{
		bonusDamage = 1.5f;
	}
	else if ((attacker->getClass()->getClassType() == Mage &&
		target->getClass()->getClassType() == Warrior))
	{
		bonusDamage = 1.5f;
	}
	actualDamage *= bonusDamage;

	target->receiveDamage(actualDamage);

	cout << "Used Assassinate on " << target->getName() << " for " << target->getStats()->getMaxHp() -
		target->getStats()->getHp() << " damage!" << endl;
	attacker->reduceMp(3);
	/*int randomPow = ((attacker->getStats()->getPow() * 0.2) - 1);
	int baseDamage = randomPow * 2.2f;

	int strongDamage = (baseDamage - target->getStats()->getDex()) * 1.5;
	int normalDamage = (baseDamage - target->getStats()->getDex());

	if (attacker->getClass()->getClassType() == Warrior &&
		target->getClass()->getClassType() == Assassin)
	{
		target->receiveDamage(strongDamage);
	}
	else if (attacker->getClass()->getClassType() == Assassin &&
		target->getClass()->getClassType() == Mage)
	{
		target->receiveDamage(strongDamage);
	}
	else if (attacker->getClass()->getClassType() == Mage &&
		target->getClass()->getClassType() == Warrior)
	{
		target->receiveDamage(strongDamage);
	}
	else
	{
		target->receiveDamage(normalDamage);
	}*/


}
