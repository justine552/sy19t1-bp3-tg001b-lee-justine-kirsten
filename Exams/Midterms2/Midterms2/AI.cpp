#include "AI.h"

AI::AI(string _name, Stats* _stats, Class* _class) : Unit(_name, _stats, _class)
{
	mName = _name;
	mStats = _stats;
	mClass = _class;

	mSkills.push_back(new BasicAttack("Basic Attack"));

	if (mClass->getClassType() == Warrior)
	{
		mSkills.push_back(new Shockwave("Shockwave"));
	}
	else if (mClass->getClassType() == Assassin)
	{
		mSkills.push_back(new Assassinate("Assassinate"));
	}
	else if (mClass->getClassType() == Mage)
	{
		mSkills.push_back(new Heal("Heal"));
	}
}

AI::AI()
{
}

AI::~AI()
{
	delete mClassType;
}

void AI::processTurn(vector<Unit*> targets, Unit* ai, int choice)
{
	// loop (keep looping while choice is invalid or cant cast skill)
	do
	{
		vector<Skill*> unitSkills = ai->getSkills();
		choice = rand() % 2;
		// check for mp
		if (ai->getStats()->getMp() < unitSkills[choice]->getMpCost())
		{
			choice = -1;
		}
	} while (choice < 0 || choice > 1);
	ai->useSkill(targets, ai, choice);
}


