#ifndef HEAL_H

#define HEAL_H
#include <string>
//#include "Skill.h"
#include "Unit.h"
using namespace std;

class Unit;

class Heal : public Skill
{
public:
	Heal();
	Heal(string name);
	~Heal();

	virtual void castSkill(Unit* attacker, vector<Unit*> targets);
private:
};
#endif
