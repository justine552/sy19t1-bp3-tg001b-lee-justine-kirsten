#include "Shockwave.h"

Shockwave::Shockwave()
{
}

Shockwave::Shockwave(string name)
{
	mName = name;
	mMpCost = 4;
}

Shockwave::~Shockwave()
{
}

void Shockwave::castSkill(Unit* attacker, vector<Unit*> targets)
{
	for (int i = targets.size() - 1; i >= 0; i--)
	{
		if (typeid(*attacker) == typeid(*targets[i]))
		{
			auto newEnd = std::remove(targets.begin(), targets.end(), targets[i]);
			targets.erase(newEnd, targets.end());
		}
	}

	for (int i = 0; i < targets.size(); i++)
	{
		int power = ((attacker->getStats()->getPow() * 0.2) - 1);
		int randomPow = rand() % power + attacker->getStats()->getPow();
		int baseDamage = randomPow * 0.9f;
		int actualDamage = (baseDamage - targets[i]->getStats()->getVit());

		if (actualDamage < 0)
			actualDamage = 0;

		int bonusDamage = 1;

		if ((attacker->getClass()->getClassType() == Warrior &&
			targets[i]->getClass()->getClassType() == Assassin))
		{
			bonusDamage = 1.5f;
		}

		actualDamage *= bonusDamage;

		targets[i]->receiveDamage(actualDamage);
		cout << "Used Shockwave on " << targets[i]->getName() << " for " << actualDamage << " damage!" << endl;
	}

	attacker->reduceMp(4);
}
