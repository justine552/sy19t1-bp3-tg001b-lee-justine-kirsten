#ifndef CLASS_H

#define CLASS_H
#include <string>
#include "Skill.h"
#include <vector>
using namespace std;

enum ClassType { Warrior, Assassin, Mage };

class Class
{
	public:
		Class();
		Class(ClassType classType);
		~Class();

		ClassType getClassType();
		string getClassName();

	private:
		ClassType mClassType;
};
#endif
