#ifndef PLAYER_H

#define PLAYER_H
#include <string>
#include "Unit.h"
//#include "Class.h"
using namespace std;

class Class;

class Player : public Unit
{
public:
	Player();
	Player(string _name, Stats* _stats, Class* _class);
	~Player();

	virtual void processTurn(vector<Unit*> targets, Unit* defender, int choice);

private:
};
#endif
