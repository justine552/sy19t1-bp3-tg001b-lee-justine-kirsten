#ifndef AI_H

#define AI_H
#include <string>
#include "Unit.h"
#include "Class.h"
using namespace std;

class AI : public Unit
{
public:
	AI(string _name, Stats* _stats, Class* _class);
	AI();
	~AI();

	virtual void processTurn(vector<Unit*> targets, Unit* ai, int choice);
private:
	Class* mClassType;
};

#endif