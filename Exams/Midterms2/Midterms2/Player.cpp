#include "Player.h"

Player::Player()
{
}

Player::Player(string _name, Stats* _stats, Class* _class) : Unit(_name, _stats, _class)
{

}

Player::~Player()
{
}

void Player::processTurn(vector<Unit*> targets, Unit* player, int choice)
{
	// loop (keep looping while choice is invalid or cant cast skill)
	do
	{
		vector<Skill*> unitSkills = player->getSkills();
		cout << "==================================" << endl;

		for (int i = 0; i < unitSkills.size(); i++)
		{
			cout << "[" << i << "] " << unitSkills[i]->getName() << "[MP Cost: " << unitSkills[i]->getMpCost() << "]" << endl;
		}
		cout << "Choose an action: " << endl;
		cin >> choice;

		// check for mp
		if (player->getStats()->getMp() < unitSkills[choice]->getMpCost())
		{
			choice = -1;
		}
	} while (choice < 0 || choice > 1);
	player->useSkill(targets, player, choice);
}
