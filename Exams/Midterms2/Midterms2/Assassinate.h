#ifndef ASSASSINATE_H

#define ASSASSINATE_H
#include <string>
#include <vector>
#include "Skill.h"
#include "Unit.h"
using namespace std;

class Assassinate : public Skill
{
	public:
		Assassinate();
		Assassinate(string name);
		~Assassinate();


		virtual void castSkill(Unit* attacker, vector<Unit*> targets);
	private:

};
#endif
