#include "Class.h"

Class::Class()
{
}

Class::Class(ClassType classType)
{
	mClassType = classType;
}

Class::~Class()
{
	delete &mClassType;
}

ClassType Class::getClassType()
{
	return mClassType;
}

string Class::getClassName()
{
	if (mClassType == Warrior)
		return "Warrior";
	else if (mClassType == Assassin)
		return "Assassin";
	else
		return "Mage";
}
