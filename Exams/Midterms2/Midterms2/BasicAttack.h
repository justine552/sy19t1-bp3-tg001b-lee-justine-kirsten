#ifndef BASICATTACK_H

#define BASICATTACK_H
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include "Skill.h"
#include "Unit.h"
using namespace std;

class BasicAttack : public Skill
{
public:
	BasicAttack();
	BasicAttack(string name);
	~BasicAttack();

	virtual void castSkill(Unit* attacker, vector<Unit*> targets);
private:
};
#endif