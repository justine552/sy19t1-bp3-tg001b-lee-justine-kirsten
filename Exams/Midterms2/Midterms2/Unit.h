#ifndef UNIT_H
#define UNIT_H

#include <string>
#include <vector>
#include <iostream>
#include "Stats.h"
#include "Class.h"
#include "Skill.h"
#include "Shockwave.h"
#include "BasicAttack.h"
#include "Heal.h"
#include "Assassinate.h"
using namespace std;

class Class;
class Skill;

class Unit
{
public:
	Unit();
	Unit(string _name, Stats* _stats, Class* _class);
	~Unit();

	Stats* getStats();
	Class* getClass();
	string getName();

	vector<Skill*> getSkills();
	void useSkill(vector<Unit*> targets, Unit* attacker, int choice);
	void receiveDamage(int dmg);
	void restoreHealth(int value);
	void reduceMp(int value);
	virtual void processTurn(vector<Unit*> targets, Unit* defender, int choice);

protected:
	Stats* mStats;
	Class* mClass;
	string mName;
	vector<Skill*> mSkills;
};
#endif
