#include "Unit.h"

Unit::Unit()
{
}

Unit::Unit(string _name, Stats* _stats, Class* _class)
{
	mName = _name;
	mStats = _stats;
	mClass = _class;

	mSkills.push_back(new BasicAttack("Basic Attack"));

	if (mClass->getClassType() == Warrior)
	{
		mSkills.push_back(new Shockwave("Shockwave"));
	}
	else if (mClass->getClassType() == Assassin)
	{
		mSkills.push_back(new Assassinate("Assassinate"));
	}
	else if (mClass->getClassType() == Mage)
	{
		mSkills.push_back(new Heal("Heal"));
	}
}

Unit::~Unit()
{
	delete mStats;
	delete mClass;
	delete &mName;
	delete &mSkills;
}

Stats* Unit::getStats()
{
	return mStats;
}

string Unit::getName()
{
	return mName;
}

vector<Skill*> Unit::getSkills()
{
	return mSkills;
}

void Unit::useSkill(vector<Unit*> targets, Unit* attacker, int choice)
{
	switch (choice)
	{
	case 0:
		mSkills[0]->castSkill(attacker, targets);
		break;
	case 1:
		mSkills[1]->castSkill(attacker, targets);
		break;
	default:
		break;
	}
}

void Unit::receiveDamage(int dmg)
{
	int newHP = getStats()->getHp() - dmg;
	getStats()->setHp(newHP);
	int hpCheck = getStats()->getHp();
	if (hpCheck <= 0)
	{
		getStats()->setMp(0);
	}
	else
	{
		getStats()->setMp(newHP);
	}
}

void Unit::restoreHealth(int value)
{
	getStats()->setHp(value);
}

void Unit::reduceMp(int value)
{
	int newMP = getStats()->getMp() - value;
	if (newMP <= 0)
	{
		getStats()->setMp(0);
	}
	else
	{
		getStats()->setMp(newMP);
	}
}

void Unit::processTurn(vector<Unit*> targets, Unit* defender, int choice)
{

}

Class* Unit::getClass()
{
	return mClass;
}
