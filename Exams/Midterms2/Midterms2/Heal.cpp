#include "Heal.h"

Heal::Heal()
{
}

Heal::Heal(string name)
{
	mName = name;
	mMpCost = 3;
}

Heal::~Heal()
{
}

void Heal::castSkill(Unit* attacker, vector<Unit*> targets)
{
	for (int i = targets.size() - 1; i >= 0; i--)
	{
		if (typeid(*attacker) != typeid(*targets[i]))
		{
			auto newEnd = remove(targets.begin(), targets.end(), targets[i]);
			targets.erase(newEnd, targets.end());
		}
	}

	Unit* target = targets[0];
	for (int i = 0; i < targets.size(); i++)
	{
		if (target->getStats()->getHp() > targets[i]->getStats()->getHp())
		{
			target = targets[i];
		}
	}
	int healHp = target->getStats()->getHp() + (target->getStats()->getHp() * 0.3);
	target->restoreHealth(healHp);
	cout << "Healed " << target->getName() << " for " << target->getStats()->getMaxHp() -
						 target->getStats()->getHp() << " health!" << endl;
	attacker->reduceMp(4);

}
