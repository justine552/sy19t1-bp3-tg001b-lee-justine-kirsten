#include "Planet.h"

using namespace std;


void Planet::addLight(SceneManager* sceneManager, Vector3 position, ColourValue diffuse, ColourValue specular)
{
	// Lecture: Adding a light source
	Light* pointLight = sceneManager->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 0.75f, 0.75f));
	//pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 0.50f));
	// Values taken from: http://www.ogre3d.org/tikiwiki/-Point+Light+Attenuation
	//pointLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
	pointLight->setAttenuation(3250, 1.0f, 0.0014, 0.000007);
	pointLight->setCastShadows(true);
}

Planet* Planet::createPlanet(SceneManager* sceneManager, const float radiusInput, ColourValue colour, string materialName, 
							bool isLit, const int nStacks, const int nSlices)
{
	//reference for making the sphere bc I can't understand any thing anymore sir :^( : http://wiki.ogre3d.org/ManualSphereMeshes
	ManualObject* manual = sceneManager->createManualObject();

	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(materialName, "General");
	myManualObjectMaterial->setReceiveShadows(true);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(isLit);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);

	manual->begin(materialName, RenderOperation::OT_TRIANGLE_LIST);
	manual->colour(colour);

	float deltaStackAngle = (Math::PI / nStacks);
	float deltaSliceAngle = (2 * Math::PI / nSlices);
	int wVerticeIndex = 0;

	// Generate the group of rings for the sphere
	for (int stack = 0; stack <= nStacks; stack++) {
		float r = radiusInput * sinf(stack * deltaStackAngle);
		float y = radiusInput * cosf(stack * deltaStackAngle);

		// Generate the group of segments for the current ring
		for (int slice = 0; slice <= nSlices; slice++) {
			float x = r * sinf(slice * deltaSliceAngle);
			float z = r * cosf(slice * deltaSliceAngle);

			// Add one vertex to the strip which makes up the sphere
			manual->position(x, y, z);
			manual->normal(Vector3(x, y, z).normalisedCopy());
			//manual->textureCoord((float)slice / (float)nSlices, (float)stack / (float)nStacks);

			if (stack != nStacks ) {
				// each vertex (except the last) has six indicies pointing to it

				manual->index(wVerticeIndex + nSlices + 1);
				manual->index(wVerticeIndex);
				manual->index(wVerticeIndex + nSlices);

				manual->index(wVerticeIndex + nSlices + 1);
				manual->index(wVerticeIndex + 1);
				manual->index(wVerticeIndex);
				wVerticeIndex++;
			}
		}; // end for slice
	} // end for stack

	manual->end();

	SceneNode* node = sceneManager->getRootSceneNode()->createChildSceneNode();
	node->attachObject(manual);
	
	return new Planet(node);
}

Planet::Planet(SceneNode* node)
{
	mNode = node;
	mLocalRotationSpeed = 0;
	mRevolutionSpeed = 0;
	speedOfTimePerCycleInSeconds = 60.0f;
}

Planet::~Planet()
{

}

void Planet::update(const FrameEvent& evt)
{
	// local rotation
	float secondPerCycle = speedOfTimePerCycleInSeconds / 60.0f;

	Degree yawDegrees = Degree(((360.0f / secondPerCycle) * mLocalRotationSpeed) * evt.timeSinceLastFrame);
	Radian yawRadian = Radian(yawDegrees);
	mNode->yaw(yawRadian);

	// Rotate the object using x and y rotations
	//mNode->rotate(Vector3(0.0, 1.0, 0.0), yawRadian);

	if (mParent)
	{
		//reference for revolution: gamefromscratch.com/post/2012/11/24/GameDev-math-recipes-Rotating-one-point-around-another-point.aspx
		float oneRevolutionInDegrees = 360.0f;
		Degree degrees = Degree(((oneRevolutionInDegrees / speedOfTimePerCycleInSeconds) * mRevolutionSpeed) * evt.timeSinceLastFrame);
		Radian radian = Radian(degrees);
		float angle = radian.valueRadians();

		Vector3 point = mNode->getPosition();
		Vector3 center = mParent->getNode().getPosition();

		float rotatedX = cos(angle) * (point.x - center.x) - sin(angle) * (point.z - center.z) + center.x;
		float rotatedZ = sin(angle) * (point.x - center.x) + cos(angle) * (point.z - center.z) + center.z;

		mNode->setPosition(rotatedX, point.y, rotatedZ);
	}
}


SceneNode& Planet::getNode()
{
	return *mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}
