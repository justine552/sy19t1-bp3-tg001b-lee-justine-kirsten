#pragma once
#include "BaseApplication.h"
#include <string> 
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;

class Planet
{
public:
	static Planet* createPlanet(SceneManager* sceneManager, const float radiusInput, ColourValue colour, std::string materialName, 
								bool isLit = true, const int nRings = 50, const int nSegments = 50);
	Planet(SceneNode* node);
	virtual ~Planet();

	void update(const FrameEvent& evt);
	void addLight(SceneManager* sceneManager, Vector3 position, ColourValue diffuse, ColourValue specular);
	SceneNode& getNode();
	void setParent(Planet* parent);
	Planet* getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);
private:
	SceneNode* mNode;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;
	float speedOfTimePerCycleInSeconds;
};

