/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h> // Remember to include these two
#include <vector>

TutorialApplication::TutorialApplication(void)
{
	planets = std::vector<Planet*>();
}
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	float defaultNumOfDaysPerRevolution = 365.20; //earth
	float defaultNumOfDaysPerRotation = 0.99726851f; //earth

	Planet* sun = Planet::createPlanet(mSceneMgr, 20, ColourValue(1, 1, 0), "SunMaterial", false);
	sun->getNode().setPosition(0, 0.5, 0);
	sun->setLocalRotationSpeed(defaultNumOfDaysPerRotation / 24.0f);
	sun->addLight(mSceneMgr, Vector3(0, 40, 0), ColourValue::White, ColourValue::White);
	
	Planet* mercury = Planet::createPlanet(mSceneMgr, 3, ColourValue(0.82f, 0.7f, 0.54f), "MercuryMaterial");
	mercury->getNode().setPosition(100, 0.5, 0);
	mercury->setParent(sun);
	mercury->setLocalRotationSpeed(defaultNumOfDaysPerRotation / 59.0f);
	mercury->setRevolutionSpeed(defaultNumOfDaysPerRevolution / 88.0f);

	Planet* venus = Planet::createPlanet(mSceneMgr, 5, ColourValue(0.93, 0.9f, 0.67f), "VenusMateria");
	venus->getNode().setPosition(200, 0.5, 0);
	venus->setParent(sun);
	venus->setLocalRotationSpeed(-defaultNumOfDaysPerRotation / 243.0f);
	venus->setRevolutionSpeed(defaultNumOfDaysPerRevolution / 224.7f);

	Planet* earth = Planet::createPlanet(mSceneMgr, 10, ColourValue::Blue, "EarthMaterial");
	earth->getNode().setPosition(300, 0.5, 0);
	earth->setParent(sun);
	earth->setLocalRotationSpeed(defaultNumOfDaysPerRotation / 0.99726851f);
	earth->setRevolutionSpeed(defaultNumOfDaysPerRevolution / 365.20f);

	Planet* moon = Planet::createPlanet(mSceneMgr, 1, ColourValue(0.7f, 0.7f, 0.7f),  "MoonMaterial");
	moon->getNode().setPosition(350, 0.5, 0);
	moon->setParent(earth);
	moon->setLocalRotationSpeed(0.0f);
	moon->setRevolutionSpeed(5.0);

	Planet* mars = Planet::createPlanet(mSceneMgr, 8, ColourValue(0.71f, 0.25f, 0.05f),  "MarsMaterial");
	mars->getNode().setPosition(450, 0.5, 0);
	mars->setParent(sun);
	mars->setLocalRotationSpeed(defaultNumOfDaysPerRotation / 1.0256944f);
	mars->setRevolutionSpeed(defaultNumOfDaysPerRevolution / 687.0f);

	planets.push_back(sun);
	planets.push_back(mercury);
	planets.push_back(venus);
	planets.push_back(earth);
	planets.push_back(moon);
	planets.push_back(mars);
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	for (int i = 0; i < planets.size(); i++)
	{
		planets[i]->update(evt);
	}
	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception & e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
