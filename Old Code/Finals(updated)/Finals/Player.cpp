#include "stdafx.h"
#include "Player.h"

Player::Player(string name, Class* playerJob, Weapon* currentWeapon, Armor* currentArmor, int gold,
	int lvl, int exp, Stats* stats)
{
	mName = name;
	mPlayerJob = playerJob;
	mCurrentWeapon = currentWeapon;
	mCurrentArmor = currentArmor;
	mGold = gold;
	mLvl = lvl;
	mExp = exp;
	mStats = stats;
	mRequiredExp = 1000;
}

void Player::attack(Monster* enemy)
{
	mHitRate = (this->mStats->mDex / enemy->mStats->mAgi) * 100;
	mDamage = ((this->mStats->mPow + this->mCurrentWeapon->mAttackPow) - (enemy->mStats->mVit));

	if (mDamage < 1)
	{
		mDamage = 1;
	}

	if (mHitRate < 1)
		mHitRate = 1;

	int randomNum = rand() % mHitRate + 1;
	if (randomNum > 80)
	{
		randomNum = 80;
	}
	if (randomNum < 20)
	{
		randomNum = 20;
	}

	if (mHitRate >= randomNum)
	{
		enemy->mStats->mHp -= this->mDamage;
		cout << this->mName << " attacked with " << this->mDamage << " damage!\n";
	}
	else
	{
		cout << this->mName << " missed!";
	}
}

void Player::rest()
{
	this->mStats->mHp = ((this->mStats->mMaxHP) - (this->mStats->mHp)) + this->mStats->mHp;
	cout << "Fully rested! Your HP is now restored.";
}

bool Player::runAway()
{
	bool didRunAway = true;
	return didRunAway;
}

void Player::addGold(Monster* monster)
{
	mGold += monster->mGold;
	cout << "Current gold: " << mGold;
}

void Player::addExp(Monster* monster)
{
	mExp += monster->mExp;
	cout << "You earned " << mExp << " exp." << endl;


	if (mExp >= mRequiredExp)
	{
		levelUp();
	}
}

void Player::levelUp()
{
	mLvl++;
	mExp -= mRequiredExp;
	mRequiredExp = mLvl * 1000;
	mStats->addRandomStat();
	cout << "You're now level " << mLvl << " !" << endl;
}