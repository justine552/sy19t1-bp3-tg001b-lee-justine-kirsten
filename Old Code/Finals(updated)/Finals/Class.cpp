#include "stdafx.h"
#include "Class.h"
#include <string>
#include <iostream>
using namespace std;

void Class::printStats(Player* player)
{{}
	cout << endl;
	cout << mClassName << endl;
	cout << "HP: "			<< mStats->mHp << endl;
	cout << "Pow: "		<< mStats->mPow << endl;
	cout << "Vit: "	<< mStats->mVit << endl;
	cout << "Agi: "		<< mStats->mAgi << endl;
	cout << "Dex: "	<< mStats->mDex << endl;
	cout << "Current Exp: " << player->mExp;
	cout << "Current Level: " << player->mLvl;
}

Class::Class(string className, Stats *stats)
{
	mStats = stats;
	mClassName = className;
}
