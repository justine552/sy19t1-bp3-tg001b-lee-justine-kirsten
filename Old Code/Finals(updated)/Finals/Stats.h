#pragma once
class Stats
{
public:
	Stats(int hp, int maxHP, int pow, int vit, int agi, int dex);
	int mHp;
	int mMaxHP;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;

	void addRandomStat();
};

