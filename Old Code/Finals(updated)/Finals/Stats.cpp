#include "stdafx.h"
#include "Stats.h"


Stats::Stats(int hp, int maxHP, int pow, int vit, int agi, int dex)
{
	mHp = hp;
	mMaxHP = maxHP;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
}

void Stats::addRandomStat()
{
	mMaxHP 	 ++;
	mPow	 ++;
	mVit	 ++;
	mAgi	 ++;
	mDex	 ++;
}
