#pragma once
#include <string>
#include <iostream>
#include "Player.h"
using namespace std;

class Player;

class Monster
{
private: 
	int	mDamage;
	int mHitRate;
public:
	Monster();
	Monster(string name, int spawnChance, int exp, int gold, Stats* stats);
	string mName;
	int mSpawnChance;
	int mExp;
	int mGold;
	Stats* mStats;

	void attackPlayer(Player* playerName);
	void printMonsterStats();
};

