#include "stdafx.h"
#include "Monster.h"
#include "Player.h"

Monster::Monster(string name, int spawnChance, int exp, int gold, Stats* stats) {
	mName = name;
	mSpawnChance = spawnChance;
	mExp = exp;
	mGold = gold;
	mStats = stats;
}

void Monster::attackPlayer(Player* playerName)
{
	mHitRate = (this->mStats->mDex/ playerName->mStats->mAgi) * 100;
	mDamage = ((this->mStats->mPow)-(playerName->mStats->mVit));

	if (mDamage < 1)
	{
		mDamage = 1;
	}

	if (mHitRate < 1)
		mHitRate = 1;

	int randomNum = rand() % mHitRate + 1;
	if (randomNum > 80)
	{
		randomNum = 80;
	}
	if (randomNum < 20)
	{
		randomNum = 20;
	}

	if (mHitRate >= randomNum)
	{
		playerName->mStats->mHp -= this->mDamage;
		cout << this->mName << " attacked with " << this->mDamage << " damage!\n";
	}
	else
	{
		cout << this->mName << " missed!";
	}
}

void Monster::printMonsterStats()
{
	cout << endl;
	cout << "=======MONSTER STATS======" << endl;
	cout << "Name: " << mName << endl;
	cout << "HP: " << mStats->mHp << endl;
	cout << "Pow: " << mStats->mPow << endl;
	cout << "Vit: " << mStats->mVit << endl;
	cout << "Agi: " << mStats->mAgi << endl;
	cout << "Dex: " << mStats->mDex << endl;
}