#pragma once
#include <string>
#include <iostream>
#include "Stats.h"
#include "Player.h"
using namespace std;

class Player;
class Class
{
public:
	Class(string className, Stats *stats);
	Stats *mStats;
	string mClassName;

	void printStats(Player* player);
};

