#pragma once
#include <string>
#include <iostream>
#include "Class.h"
#include "Monster.h"
#include "Weapon.h"
#include "Armor.h"
#include "Stats.h"

using namespace std;

class Class;
class Monster;
class Player
{
private:
	int mDamage;
	int mHitRate;
public:
	Player(string name, Class* playerJob, Weapon* currentWeapon, Armor* currentArmor, int gold,
		int lvl, int mExp, Stats* stats);
	Class* mPlayerJob;
	Weapon* mCurrentWeapon;
	Armor* mCurrentArmor;
	string mName;
	int mGold;
	int mLvl;
	int mExp;
	int mRequiredExp;
	Stats* mStats;
	
	void attack(Monster* enemy);
	void rest();
	bool runAway();
	void addExp(Monster* monster);
	void addGold(Monster* monster);
	void levelUp();
};