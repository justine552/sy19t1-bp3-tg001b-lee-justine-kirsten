#include "stdafx.h"
#include "Map.h"

void Map::printCoordinates()
{
	cout << "(" << x << ", " << y << ")";
}

void Map::moveNorth()
{
	y += 1;
}

void Map::moveSouth()
{
	y -= 1;
}

void Map::moveEast()
{
	x += 1;
}

void Map::moveWest()
{
	x -= 1;
}
