#include "stdafx.h"
#include <string>
#include <iostream>
#include <ctime>
#include <conio.h>
#include "Class.h"
#include "Monster.h"
#include "Finals.h"
#include "Stats.h"
#include "Player.h"
#include "Weapon.h"
#include "Armor.h"
#include "Map.h"
#include "Shop.h"

using namespace std;

string inputName()
{
	string name;
	cout << "Name: ";
	cin >> name;

	return name;
}

Class* getClass(int choice)
{
	switch (choice)
	{
	case 1:
		return new Class("Warrior", new Stats(100, 100, 15, 15, 10, 10));
	case 2:
		return new Class("Thief", new Stats(100, 100, 10, 10, 15, 15));
	case 3:
		return new Class("Crusader", new Stats(150, 150, 10, 15, 10, 10));
	default:
		return NULL;
	}
}

int inputClassChoice()
{
	int choice = 0;

	do
	{
		cout << "Pick a class:" << endl;
		cout << "[1] Warrior" << endl;
		cout << "[2] Thief" << endl;
		cout << "[3] Crusader" << endl;
		cout << "Choice: ";
		cin >> choice;
	} while (choice <= 0 && choice >= 4);

	return choice;
}

Player* createPlayer()
{
	string name = inputName();
	int classChoice = inputClassChoice();
	Class* chosenClass = getClass(classChoice);

	Weapon* trainingSword = new Weapon("Training Sword", 8);
	Armor* trainingClothes = new Armor("Training Clothes", 8);

	return new Player(name, chosenClass, trainingSword, trainingClothes, 0, 1, 0, chosenClass->mStats);

}

Monster* spawnMonster()
{
	Monster** monster = new Monster*[4];
	monster[0] = (new Monster("Goblin", 25, 10, 10, new Stats(100, 100, 10, 10, 10, 10)));
	monster[1] = (new Monster("Ogre", 25, 250, 50, new Stats(150, 125, 15, 15, 15, 15)));
	monster[2] = (new Monster("Orc", 25, 500, 100, new Stats(200, 200, 20, 20, 20, 20)));
	monster[3] = (new Monster("Orc Lord", 5, 1000, 1000, new Stats(300, 300, 25, 25, 25, 25)));

	int noSpawnChance = 20;
	int allSpawnChance = 0;

	for (int i = 0; i < 4; i++)
	{
		allSpawnChance += monster[i]->mSpawnChance;
	}

	allSpawnChance += noSpawnChance;

	for (int i = 0; i < 4; i++)
	{
		int randomNum = rand() % allSpawnChance;

		if (randomNum < monster[i]->mSpawnChance)
		{
			return monster[i];
		}
		else
		{
			allSpawnChance -= monster[i]->mSpawnChance;
		}
	}

	return NULL;
	/*int randomNum = rand() % 100 + 1;

	if (randomNum >= 1 && randomNum <= 20)
	{
		return NULL;
	}
	else if (randomNum >= 21 && randomNum <= 45)
	{
		return new Monster("Goblin", 25, 100, 1000, new Stats(100, 100, 10, 10, 10, 10));
	}
	else if (randomNum >= 46 && randomNum <= 70)
	{
		return new Monster("Ogre", 25, 250, 5000, new Stats(150, 125, 15, 15, 15, 15));
	}
	else if (randomNum >= 71 && randomNum < 95)
	{
		return new Monster("Orc", 25, 500, 10000, new Stats(200, 200, 20, 20, 20, 20));
	}
	else if (randomNum >= 96 && randomNum <= 100)
	{
		return new Monster("Orc Lord", 5, 1000, 1000, new Stats(300, 300, 25, 25, 25, 25));
	}*/
}

int inputPlayerAction(Map* currentMap)
{
	int choice = 0;

	do
	{
		cout << "Player positon: ";
		currentMap->printCoordinates();
		cout << endl;

		cout << "[1] Move" << endl;
		cout << "[2] Rest" << endl;
		cout << "[3] ViewStats" << endl;
		cout << "[4] Quit" << endl;
		cout << "Choice: ";
		cin >> choice;
		system("cls");
	} while (choice <= 0 && choice >= 5);

	return choice;
}

int inputMovementDirection(Map* currentMap)
{
	int choice = 0;

	do
	{
		cout << "Player positon: ";
		currentMap->printCoordinates();
		cout << endl;

		cout << "[1] North" << endl;
		cout << "[2] South" << endl;
		cout << "[3] East" << endl;
		cout << "[4] West" << endl;
		cout << "Choice: ";
		cin >> choice;
	} while (choice <= 0 && choice >= 5);

	return choice;
}

Map* createMap()
{
	return new Map();
}

void processMovementDirection(int movementDirection, Map* currentMap)
{
	switch (movementDirection)
	{
	case 1:
		currentMap->moveNorth();
		break;
	case 2:
		currentMap->moveSouth();
		break;
	case 3:
		currentMap->moveEast();
		break;
	case 4:
		currentMap->moveWest();
		break;
	default:
		break;
	}
}

bool startBattle(Map* currentMap, Player* player, Monster* monster)
{
	int choice = 0;
	cout << "Battle Start" << endl;
	cout << "[1] Attack  [2] Run Away" << endl;
	cin >> choice;
	cout << endl;
	if (choice == 1)
	{
		player->attack(monster);
		monster->attackPlayer(player);
	}
	else if (choice == 2)
	{

		int randomNum = rand() % 100 + 1;

		if (randomNum <= 25)
		{
			player->runAway();
			cout << "You have successfully ran away." << endl;
			system("cls");
			return false;
		}
		else
		{
			cout << "Failed to run away and got hit" << endl;
			monster->attackPlayer(player); 
		}

	}

	return true;
}

void evaluateState(Map* currentMap, Player* player)
{
	Monster* monster = spawnMonster();
	int gold = 0;
	int exp = 0;
	if (monster == NULL)
	{
		// do nothing
		// no monster
		return;
	}
	else
	{
		while (player->mStats->mHp > 0 && monster->mStats->mHp > 0)
		{
			monster->printMonsterStats();
			bool isBattling = startBattle(currentMap, player, monster);

			if (isBattling == false)
				return;

			if (monster->mStats->mHp <= 0)
			{
				cout << monster->mName << " has been defeated." << endl;
				player->addGold(monster);
				player->addExp(monster);
				system("pause");
				system("cls");
			}
			else if (player->mStats->mHp <= 0)
			{
				cout << "You died." << endl;
				break;
			}
		}
	}
}

void processPlayerAction(int playerAction, Map* currentMap, Player* player)
{
	int directionChoice = 0;
	switch (playerAction)
	{
	case 1:
		directionChoice = inputMovementDirection(currentMap);
		processMovementDirection(directionChoice, currentMap);
		evaluateState(currentMap, player);
		break;
	case 2:
		player->rest();
		break;
	case 3:
		player->mPlayerJob->printStats(player);
		break;
	case 4:
		break;
	default:
		break;
	}
	_getch();
}

int main()
{
	srand(time(NULL));

	Map* map = createMap();
	Player* player = createPlayer();
	bool isGameRunning = true;
	int playerAction = 0;

	while (playerAction != 4 && player->mStats->mHp > 0)
	{
		system("cls");
		playerAction = inputPlayerAction(map);
		processPlayerAction(playerAction, map, player);
	}

	_getch();

	return 0;
}

