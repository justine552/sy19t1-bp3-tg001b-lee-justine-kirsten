#include "NormalAttack.h"

NormalAttack::NormalAttack(string name)
{
	mName = name;
}

string NormalAttack::getName()
{
	return mName;
}

void NormalAttack::useSkill(Character* player, Character* target)
{
	target->receiveDamage(player->getCurrentWeapon()->getDamage());
	cout << player->getCurrentWeapon()->getDamage() << " damage dealt to target.";
}
