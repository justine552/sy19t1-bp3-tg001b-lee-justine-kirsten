#pragma once
#include <string>
#include <iostream>
#include "Character.h"
#include "Weapon.h"
#include "Skill.h"
using namespace std;

class Character;
class Weapon;
class Syphon: public Skill
{
	public:
		Syphon(string name);

		string getName();
		void useSkill(Character* player, Character* target);
	private:
		string mName;
};

