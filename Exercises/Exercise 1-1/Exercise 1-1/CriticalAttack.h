#pragma once
#include <string>
#include <iostream>
#include "Character.h"
#include "Weapon.h"
#include "Skill.h"
using namespace std;

class Weapon;
class Character;
class CriticalAttack: public Skill
{
	public:
		CriticalAttack(string name);

		string getName();
		void useSkill(Character* player, Character* target);
	private:
		string mName;
};

