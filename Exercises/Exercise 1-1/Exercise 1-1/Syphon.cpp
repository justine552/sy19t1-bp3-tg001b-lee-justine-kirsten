#include "Syphon.h"

Syphon::Syphon(string name)
{
	mName = name;
}

string Syphon::getName()
{
	return mName;
}

void Syphon::useSkill(Character* player, Character* target)
{
	target->receiveDamage(player->getCurrentWeapon()->getDamage());
	cout << player->getCurrentWeapon()->getDamage() << " damage dealt to target.";
	player->replenishHp = player->getCurrentWeapon()->getDamage() * 0.25;
}
