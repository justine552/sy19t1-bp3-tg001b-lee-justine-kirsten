#pragma once
#include <string>
#include <iostream>
#include "Character.h"
#include "NormalAttack.h"
#include "CriticalAttack.h"
#include "Syphon.h"
#include "Vengeance.h"
#include "DopelBlade.h"
using namespace std;

class Character;
class Skill
{
	Skill(int mpCost, string name);
	public:
		void cast(Character* caster, Character* target);
	protected:
		int mMpCost;
		string mName;
};

