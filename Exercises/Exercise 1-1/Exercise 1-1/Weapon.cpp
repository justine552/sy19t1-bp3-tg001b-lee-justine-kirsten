#include "Weapon.h"

Weapon::Weapon(string name, int damage)
{
	mName = name;
	mDamage = damage;
}

string Weapon::getName()
{
	return mName;
}

int Weapon::getDamage()
{
	return mDamage;
}
