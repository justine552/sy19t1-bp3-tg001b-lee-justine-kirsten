#pragma once
#include <string>
#include <iostream>
#include "Weapon.h"
#include "Character.h"
#include "Skill.h"
using namespace std;

class Weapon;
class Character;
class DopelBlade: public Skill
{
	public:
		DopelBlade(string name);

		string getName();
		void useSkill(Character* player, Character* target);
	private:
		string mName;
};

