#include "Vengeance.h"

Vengeance::Vengeance(string name)
{
	mName = name;
}

string Vengeance::getName()
{
	return mName;
}

void Vengeance::useSkill(Character* player, Character* target)
{
	target->receiveDamage(player->getCurrentWeapon()->getDamage() + ((player->getHp * 0.25) * 2));
	cout << player->getCurrentWeapon()->getDamage() + ((player->getHp * 0.25) * 2) << " damage dealt." << endl;
	if (player->getHp <= 1)
	{
		player->getHp = 1;
	}

}
