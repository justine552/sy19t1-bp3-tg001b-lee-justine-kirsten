#pragma once
#include <iostream>
#include <string>
using namespace std;

class Weapon
{
	public:
		Weapon(string name, int damage);

		string getName();
		int getDamage();
	private:
		string mName;
		int mDamage;
};

