#pragma once
#include <string>
#include <iostream>
#include "Character.h"
#include "Weapon.h"
#include "Skill.h"
using namespace std;

class Weapon;
class Character;
class NormalAttack : public Skill
{
	public:
		NormalAttack(string name);

		string getName();
		void useSkill(Character* player, Character* target);
	private:
		string mName;
};

