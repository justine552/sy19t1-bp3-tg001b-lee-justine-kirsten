#include "Character.h"

Character::Character(int hp, int mp, Weapon* currentWeapon)
{
	mHp = hp;
	mMp = mp;
	mCurrentWeapon = currentWeapon;
}

void Character::attack(Character* target)
{
	target->receiveDamage(this->getCurrentWeapon()->getDamage());
	cout << this->getCurrentWeapon()->getDamage() << " damage dealt to target.";
}

int Character::getHp()
{
	return mHp;
}

int Character::getMp()
{
	return mMp;
}

void Character::receiveDamage(int damage)
{
	mHp -= damage;
}

void Character::replenishHp(int hp)
{
	mHp += hp;
}

Weapon* Character::getCurrentWeapon()
{
	return mCurrentWeapon;
}

Skill* Character::getRandSkill(NormalAttack* normAttack, CriticalAttack* critAttack, Syphon* syphon,
									Vengeance* vengeance, DopelBlade* dopelBlade, Character* player, Character* target)
{
	int randChance = rand() % 5 + 1;
	
	if (randChance == 1)
	{
		normAttack->useSkill(player, target);
	}
	else if (randChance == 2)
	{
		critAttack->useSkill(player, target);
	}
	else if (randChance == 3)
	{
		syphon->useSkill(player, target);
	}
	else if (randChance == 4)
	{
		vengeance->useSkill(player, target);
	}
	else if (randChance == 5)
	{
		dopelBlade->useSkill(player, target);
	}
	//switch (randChance)
	//{
	//case 1:
	//	normAttack->useSkill;
	//	break;
	//case 2:
	//	critAttack->useSkill;
	//	break;
	//case 3:
	//	syphon->useSkill;
	//	break;
	//case 4:
	//	vengeance->useSkill;
	//	break;
	//case 5:
	//	dopelBlade->useSkill;
	//	break;
	//default:
	//	break;
	//}

}

