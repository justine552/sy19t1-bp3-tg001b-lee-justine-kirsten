#pragma once
#include <iostream>
#include <string>
#include <conio.h>
#include "Weapon.h"
#include "CriticalAttack.h"
#include "NormalAttack.h"
#include "Syphon.h"
#include "DopelBlade.h"
#include "Vengeance.h"
#include "Skill.h"
#include <vector>
using namespace std;

class NormalAttack;
class CriticalAttack;
class Syphon;
class DopelBlade;
class Vengeance;
class Skill;
class Weapon;
class Character
{
	public:
		Character(int hp, int mp, Weapon* currentWeapon);

		void attack(Character* target);
		int getHp();
		int getMp();
		void receiveDamage(int damage);
		void replenishHp(int hp);
		Weapon* getCurrentWeapon();
		Skill* getRandSkill(NormalAttack* normAttack, CriticalAttack* critAttack, Syphon* syphon, Vengeance* vengeance, DopelBlade* dopelBlade, Character* player, Character* target);
	private:
		int mHp;
		int mMp;
		Weapon* mCurrentWeapon;
		vector<Skill*> skills;
};

