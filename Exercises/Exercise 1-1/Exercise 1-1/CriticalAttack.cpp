#include "CriticalAttack.h"

CriticalAttack::CriticalAttack(string name)
{
	mName = name;
}

string CriticalAttack::getName()
{
	return mName;
}

void CriticalAttack::useSkill(Character* player, Character* target)
{
	int randChance = rand() % 100 + 1;
	if (randChance <= 20)
	{
		target->receiveDamage(player->getCurrentWeapon()->getDamage() * 2);
		cout << player->getCurrentWeapon()->getDamage() * 2 << " damage dealt to target.";
	}
	else
	{
		target->receiveDamage(player->getCurrentWeapon()->getDamage());
		cout << player->getCurrentWeapon()->getDamage() << " damage dealt to target.";
	}


}
