#include <iostream>
#include <string>
#include <conio.h>
#include <ctime>
#include "Character.h"
#include "Weapon.h"
#include "NormalAttack.h"
#include "CriticalAttack.h"
#include "Syphon.h"
#include "Vengeance.h"
#include "DopelBlade.h"
using namespace std;

void printStats(Character* player)
{
	cout << "Player 1" << endl;
	cout << "HP: " << player->getHp << endl;
	cout << "MP: " << player->getMp << endl;
	cout << "Weapon: " << player->getCurrentWeapon << endl;
	cout << endl;
}
int main()
{
	srand(time(NULL));
	Character* player1 = new Character(200, 200, new Weapon("Blazing Sword", 30));
	Character* player2 = new Character(200, 200, new Weapon("Chilling Staff", 30));

	while (player1->getHp > 0 || player2->getHp > 0)
	{
		printStats(player1);
		printStats(player2);
		cout << "====================================" << endl;
		player1->attack(player2);
	}



	return 0;
}
