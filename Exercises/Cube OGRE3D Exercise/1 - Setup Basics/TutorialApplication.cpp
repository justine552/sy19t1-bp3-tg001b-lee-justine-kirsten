/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

#include "TutorialApplication.h"
#include <OgreManualObject.h>
using namespace Ogre;

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	//create a manual object
	ManualObject* object = mSceneMgr->createManualObject();

	//begin drawing
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//draw your object here
	//front square
	object->position(0, 10, 0);
	object->position(0, 0, 0);
	object->position(10, 0, 0);

	object->position(10, 10, 0);
	object->position(0, 10, 0);
	object->position(10, 0, 0);

	//back square
	object->position(10, 0, -10);
	object->position(0, 0, -10);
	object->position(0, 10, -10);

	object->position(10, 0, -10);
	object->position(0, 10, -10);
	object->position(10, 10, -10);

	//left square
	object->position(0, 10, -10);
	object->position(0, 0, -10);
	object->position(0, 0, 0);

	object->position(0, 10, -10);
	object->position(0, 0, 0);
	object->position(0, 10, 0);

	//right square
	object->position(10, 10, 0);
	object->position(10, 0, 0);
	object->position(10, 0, -10);

	object->position(10, 10, 0);
	object->position(10, 0, -10);
	object->position(10, 10, -10);

	//top square
	object->position(10, 10, 0);
	object->position(0, 10, -10);
	object->position(0, 10, 0);

	object->position(10, 10, 0);
	object->position(10, 10, -10);
	object->position(0, 10, -10);

	//bottom square
	object->position(10, 0, -10);
	object->position(0, 0, 0);
	object->position(0, 0, -10);

	object->position(10, 0, -10);
	object->position(10, 0, 0);
	object->position(0, 0, 0);

	//end drawing
	object->end();

	// add the manual object to the scene
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object);

	// Create your scene here
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
