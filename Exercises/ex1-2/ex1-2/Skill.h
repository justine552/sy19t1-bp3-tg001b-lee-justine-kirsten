#pragma once
#include <string>
#include <iostream>
using namespace std;

class Character;
class Skill
{
	public:
		Skill(string name, int effect);
		~Skill();


		virtual string getName();
		virtual int getEffect();
		// Copy this function to all child classes
		virtual void execute(Character* caster, Character* target);
		// virtual means a function can be OVERWRITTEN by child classes

	// protected means these variables are PRIVATE outside but can be inherited
	protected:
		string mName;
		int mEffect;
	};

