#include "Skill.h"

Skill::Skill(string name, int effect)
{
	mName = name;
	mEffect = effect;
}

Skill::~Skill()
{
}

string Skill::getName()
{
	return mName;
}

int Skill::getEffect()
{
	return mEffect;
}

void Skill::execute(Character* caster, Character* target)
{
	cout << "Skill used! " << endl;
}
