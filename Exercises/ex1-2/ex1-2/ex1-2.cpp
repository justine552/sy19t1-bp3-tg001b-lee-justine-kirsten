#include <iostream>
#include <string>
#include <conio.h>
#include <ctime>
#include <vector>
#include "Skill.h"
#include "Vengeance.h"
#include "Character.h"
#include "Syphon.h"
using namespace std;

int main()
{
	srand(time(NULL));
	Character* player = new Character();
	Character* enemy = new Character();

	
	int randomIndex = rand() % 2;

	// Get a random skill from the vector
	// Version 1: One liner
	skills[randomIndex]->execute(player, enemy);

	randomIndex = rand() % 2;
	// Version 2: Have a variable
	Skill* randomSkill = skills[randomIndex];
	randomSkill->execute(player, enemy);

	system("pause");
	return 0;
}

