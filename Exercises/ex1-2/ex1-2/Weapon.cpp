#include "Weapon.h"

Weapon::Weapon(int damage)
{
	mDamage = damage;
}

Weapon::~Weapon()
{
}

int Weapon::getDamage()
{
	return mDamage;
}
