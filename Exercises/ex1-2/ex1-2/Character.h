#pragma once
#include <iostream>
#include <string>
#include <time.h>
#include "Skill.h"
#include "Weapon.h"
#include <vector>
using namespace std;

class Character
{
	public:
		Character();
		Character(int hp, int mp);
		~Character();

		int getHp();
		int getMp();

		virtual void addSkill();
	/*	vector<Skill*> skillsCopy();*/
	private:	
		vector<Skill*> skills;
		Weapon* weapon;
		int mHp;
		int mMp;
};

