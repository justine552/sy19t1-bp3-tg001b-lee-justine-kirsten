#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class CritAttack
{
	public:
		CritAttack();
		~CritAttack();

		// Same function from parent class
		void execute(Character* caster, Character* target);
	private:
};

