#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class Syphon : public Skill
{
	public:
		Syphon();
		~Syphon();

		// Same function from parent class
		void execute(Character* caster, Character* target);
};

