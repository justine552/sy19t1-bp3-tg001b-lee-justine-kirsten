#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class DopelBlade : public Skill
{
	public:
		DopelBlade();
		~DopelBlade();

		// Same function from parent class
		void execute(Character* caster, Character* target);
	private:
};

