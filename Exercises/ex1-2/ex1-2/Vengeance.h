#pragma once
#include <string>
#include "Skill.h"
#include "Character.h"
using namespace std;

class Vengeance
{
	public:
		Vengeance();
		~Vengeance();

		// Same function from parent class
		virtual void execute(Character* caster, Character* target);
	private:

};

