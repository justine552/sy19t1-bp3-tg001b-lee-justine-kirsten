#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class NormAttack
{
	public:
		NormAttack();
		~NormAttack();

		// Same function from parent class
		void execute(Character* caster, Character* target);
	private:
};

