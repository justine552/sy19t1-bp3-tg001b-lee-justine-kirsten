#pragma once
#include <iostream>
#include <string>
using namespace std;

class Weapon
{
	public:
		Weapon(int damage);
		~Weapon();

		int getDamage();
	private:
		int mDamage;
};

