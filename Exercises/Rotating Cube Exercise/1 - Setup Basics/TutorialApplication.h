/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"

using namespace Ogre;
//---------------------------------------------------------------------------

class TutorialApplication : public BaseApplication
{
public:
	TutorialApplication(void);
	virtual ~TutorialApplication(void);

protected:
	virtual void createScene(void);
	// Sample movement code
	// bool keyPressed(const OIS::KeyEvent &arg);
	bool frameStarted(const FrameEvent& evt);
	bool numPressed8(const OIS::KeyEvent& arg);
	bool numReleased8(const OIS::KeyEvent& arg);
	bool numPressed2(const OIS::KeyEvent& arg);
	bool numReleased2(const OIS::KeyEvent& arg);
	bool numPressed4(const OIS::KeyEvent& arg);
	bool numReleased4(const OIS::KeyEvent& arg);
	bool numPressed6(const OIS::KeyEvent& arg);
	bool numReleased6(const OIS::KeyEvent& arg);

private:
	SceneNode* cubeNode;
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
