/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h> // Remember to include these two


TutorialApplication::TutorialApplication(void)
{
}
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// First you have to create an object in the scene
	ManualObject* object = mSceneMgr->createManualObject(); // don't do new ManualObject()
	// Begin drawing the object
	// The following code means: 
	// Display my object without any lighting
	// and with a base color of white
	// Using a triangle list (3 vertices = 1 triangle)
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//draw your object here
	object->position(-10, -10, 10);
	object->position(10, -10, 10);
	object->position(10, 10, 10);
	object->position(10, 10, 10);
	object->position(-10, 10, 10);
	object->position(-10, -10, 10);

	//back square
	object->position(10, -10, -10);
	object->position(-10, -10, -10);
	object->position(-10, 10, -10);
	object->position(-10, 10, -10);
	object->position(10, 10, -10);
	object->position(10, -10, -10);

	//left square
	object->position(-10, -10, -10);
	object->position(-10, -10, 10);
	object->position(-10, 10, 10);
	object->position(-10, 10, 10);
	object->position(-10, 10, -10);
	object->position(-10, -10, -10);

	//right square
	object->position(10, -10, 10);
	object->position(10, -10, -10);
	object->position(10, 10, -10);
	object->position(10, 10, -10);
	object->position(10, 10, 10);
	object->position(10, -10, 10);

	//top square
	object->position(-10, 10, 10);
	object->position(10, 10, 10);
	object->position(10, 10, -10);
	object->position(10, 10, -10);
	object->position(-10, 10, -10);
	object->position(-10, 10, 10);

	//bottom square
	object->position(10, -10, 10);
	object->position(-10, -10, 10);
	object->position(-10, -10, -10);
	object->position(-10, -10, -10);
	object->position(10, -10, -10);
	object->position(10, -10, 10);

	// After drawing, you need to END the drawing
	object->end();
	// After ending the drawing, you need to add the object to the scene
	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(object);
}

// Sample movement code
bool TutorialApplication::numPressed8(const OIS::KeyEvent &arg)
{
	if (arg.key == OIS::KC_NUMPAD8)
	{
		cubeNode->translate(30, 0, 0);
	}

	return true;
}

bool TutorialApplication::numReleased8(const OIS::KeyEvent& arg)
{
	if (arg.key == OIS::KC_NUMPAD8)
	{
		cubeNode->translate(30, 0, 0);
	}

	return true;
}

bool TutorialApplication::numPressed2(const OIS::KeyEvent& arg)
{
	if (arg.key == OIS::KC_L)
	{
		cubeNode->translate(-30, 0, 1);
	}

	return true;
}

bool TutorialApplication::numReleased2(const OIS::KeyEvent& arg)
{
	if (arg.key == OIS::KC_L)
	{
		cubeNode->translate(-30, 0, 0);
	}

	return true;
}

bool TutorialApplication::numPressed4(const OIS::KeyEvent& arg)
{
	if (arg.key == OIS::KC_L)
	{
		cubeNode->translate(0, 30, 0);
	}

	return true;
}

bool TutorialApplication::numReleased4(const OIS::KeyEvent& arg)
{
	if (arg.key == OIS::KC_L)
	{
		cubeNode->translate(0, 30, 0);
	}

	return true;
}

bool TutorialApplication::numPressed6(const OIS::KeyEvent& arg)
{
	if (arg.key == OIS::KC_L)
	{
		cubeNode->translate(0, -30, 0);
	}

	return true;
}

bool TutorialApplication::numReleased6(const OIS::KeyEvent& arg)
{
	if (arg.key == OIS::KC_L)
	{
		cubeNode->translate(0, -30, 0);
	}

	return true;
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{

	Degree x;
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8)) {
		x += Degree(30 * evt.timeSinceLastFrame);
	}
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2)) {
		x -= Degree(30 * evt.timeSinceLastFrame);
	}

	Degree y;
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4)) {
		y -= Degree(30 * evt.timeSinceLastFrame);
	}
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6)) {
		y += Degree(30 * evt.timeSinceLastFrame);
	}
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
